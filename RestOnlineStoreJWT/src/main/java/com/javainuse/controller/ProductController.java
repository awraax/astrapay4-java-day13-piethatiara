package com.javainuse.controller;

import com.javainuse.dao.ProductServiceImpl;

import com.javainuse.model.DaoProduct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;


@RestController
public class ProductController
{
    public static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductServiceImpl productServiceImpl;

    // -------------------Retrieve All DAOProducts--------------------------------------------

    @RequestMapping(value = "/productrepo/", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<DaoProduct>> listAllDAOProducts() {
        List<DaoProduct> products = (List<DaoProduct>) productServiceImpl.findAll();
        if (products.isEmpty()) {
            return new ResponseEntity<>(products, HttpStatus.NOT_FOUND);
        }
        productServiceImpl.findAll();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    // -------------------Retrieve Single DAOProduct------------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getDAOProduct(@PathVariable("id") int id) {
        logger.info("Fetching DAOProduct with id {}", id);
        DaoProduct product = productServiceImpl.findById(id).get();

        return new ResponseEntity<>(product, HttpStatus.OK);
    }


    // -------------------Create a DAOProduct-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createDAOProductNew(@RequestBody DaoProduct product) throws SQLException, ClassNotFoundException {
        logger.info("Creating DAOProduct : {}", product);

        productServiceImpl.save(product);

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }


    // ------------------- Update a DAOProduct ------------------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateDAOProduct(@PathVariable("id") int id, @RequestBody DaoProduct product) {
        logger.info("Updating DAOProduct with id {}", id);

        DaoProduct currentDAOProduct = productServiceImpl.findById(id).get();

        currentDAOProduct.setName(product.getName());
        currentDAOProduct.setCategoryId(product.getCategoryId());
        currentDAOProduct.setPrice(product.getPrice());
        currentDAOProduct.setId(id);

        productServiceImpl.save(currentDAOProduct);
//        productServiceHM.updateDAOProduct(currentDAOProduct);
        return new ResponseEntity<>(currentDAOProduct, HttpStatus.OK);
    }

    // ------------------- Delete a DAOProduct-----------------------------------------

    // ------------------- Delete a DAOProduct-----------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDAOProduct(@PathVariable("id") int id) {
        logger.info("Fetching & Deleting DAOProduct with id {}", id);

        DaoProduct product = productServiceImpl.findById(id).get();
//        if (product == null) {
//            logger.error("Unable to delete. DAOProduct with id {} not found.", id);
//            return new ResponseEntity<>(new CustomErrorType("Unable to delete. DAOProduct with id " + id + " not found."),
//                    HttpStatus.NOT_FOUND);
//        }
        productServiceImpl.deleteById(id);
        return new ResponseEntity<DaoProduct>(HttpStatus.NO_CONTENT);
    }

    // ------------------- Delete All DAOProducts-----------------------------

    @RequestMapping(value = "/productrepo/", method = RequestMethod.DELETE)
    public ResponseEntity<DaoProduct> deleteAllDAOProducts() {
        logger.info("Deleting All DAOProducts");

        productServiceImpl.deleteAll();
        return new ResponseEntity<DaoProduct>(HttpStatus.NO_CONTENT);
    }
}
