package com.javainuse.dao;

import com.javainuse.model.DaoProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductServiceImpl extends CrudRepository<DaoProduct, Integer> {
//    DAOProduct findByUsername(String username);
}