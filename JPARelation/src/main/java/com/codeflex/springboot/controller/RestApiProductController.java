package com.codeflex.springboot.controller;

import com.codeflex.springboot.entity.Product;
import com.codeflex.springboot.entity.ProductDet;
import com.codeflex.springboot.repo.ProductRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class RestApiProductController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiProductController.class);

    @Autowired
    private ProductRepo productRepo;

    // -------------------Create a Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createProductNew(@RequestBody Map<String, Object> payload) throws SQLException, ClassNotFoundException {
        //logger.info("Creating Product : {}", product);
        //logger.info("Creating Product : {}", product.getName());

        Product product = new Product();
        product.setName(payload.get("name").toString());
        product.setCategoryId(Integer.parseInt(payload.get("categoryId").toString()));
        product.setPrice(Double.parseDouble(payload.get("price").toString()));

        int i = 0;

        @SuppressWarnings("unchecked")
        List<Map<String, Object>> prodDet = (List<Map<String, Object>>) payload.get("productDet");
        for (Map<String, Object> prodDetObj : prodDet) {
            logger.info("Adding Detail : {}", prodDetObj.get("ket").toString());
            ProductDet productDet = new ProductDet();
            productDet.setKet(prodDetObj.get("ket").toString());
            product.addProdDet(productDet);
        }

        productRepo.save(product);

       /*int idGet = ((int) productRepo.save(product).getId());

        logger.info("Creating Product idGet : {}", idGet);
        //logger.info("Creating Product Detail : {}", product.getProductDet());

        productRepo.save(product);*/

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }

// -------------------Update a Product-------------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.PUT, produces="application/json")
    public ResponseEntity<?> updateProduct(@PathVariable("id") int id, @RequestBody Map<String, Object> payload) throws SQLException, ClassNotFoundException {
        logger.info("Updating product : {}", id);

        Product prd = productRepo.findById(id).get();

        prd.setName(payload.get("name").toString());
        prd.setCategoryId((Integer) payload.get("categoryId"));
        prd.setPrice(Double.parseDouble(payload.get("price").toString()));

        int i = 0;

        @SuppressWarnings("unchecked")
        List<Map<String, Object>> prodDet = (List<Map<String, Object>>) payload.get("productDet");
        for (Map<String, Object> prodDetObj : prodDet) {
            logger.info("Adding Detail : {}", prodDetObj.get("ket").toString());
            ProductDet productDet = new ProductDet();
            long idDetNew = productDet.getIddet();
            productDet.setIddet(idDetNew);
            productDet.setKet(prodDetObj.get("ket").toString());
            prd.addProdDet(productDet);
        }

        productRepo.save(prd);

        return new ResponseEntity<>(prd, HttpStatus.CREATED);
    }

    // ------------------- Delete a DAOProduct-----------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDAOProduct(@PathVariable("id") int id) {
        logger.info("Fetching & Deleting Product with id {}", id);

        Product product = productRepo.findById(id).get();
//        if (product == null) {
//            logger.error("Unable to delete. DAOProduct with id {} not found.", id);
//            return new ResponseEntity<>(new CustomErrorType("Unable to delete. DAOProduct with id " + id + " not found."),
//                    HttpStatus.NOT_FOUND);
//        }
        productRepo.deleteById(id);
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }

    // ------------------- Delete All DAOProducts-----------------------------

    @RequestMapping(value = "/productrepo/", method = RequestMethod.DELETE)
    public ResponseEntity<Product> deleteAllDAOProducts() {
        logger.info("Deleting All Product");

        productRepo.deleteAll();
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }

    // -------------------Retrieve All DAOProducts--------------------------------------------

    @RequestMapping(value = "/productrepo/", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<Product>> listAllDAOProducts() {
//        List<DAOProduct> products = productServiceHM.findAllDAOProducts();
        List<Product> products = (List<Product>) productRepo.findAll();
        if (products.isEmpty()) {
            return new ResponseEntity<>(products, HttpStatus.NOT_FOUND);
        }
        productRepo.findAll();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    // -------------------Retrieve Single DAOProduct------------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getDAOProduct(@PathVariable("id") int id) {
        logger.info("Fetching Product with id {}", id);
        Product product = productRepo.findById(id).get();
//        if (product == null) {
//            logger.error("DAOProduct with id {} not found.", id);
//            return new ResponseEntity<>(new CustomErrorType("DAOProduct with id " + id  + " not found"), HttpStatus.NOT_FOUND);
//        }
//        productServiceDB.findById(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }


}