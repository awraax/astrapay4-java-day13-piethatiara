package com.codeflex.springboot.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    @Column(name = "category_id")
    private int categoryId;

    private double price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("product")
    List<ProductDet> productDets = new ArrayList<>();

    public void addProdDet(ProductDet prodDet) {
        productDets.add(prodDet);
        prodDet.setProduct(this);
    }

    public void removeProdDet(ProductDet prodDet) {
        productDets.remove(prodDet);
        //prodDet.setProduct(null);
    }

    public Product(){

    }

    public Map<String, Object> getProductDet() {
        return (Map<String, Object>) productDets;
    }

    public void setProductDet(List<ProductDet> productDet) {
        this.productDets = productDet;
    }
}